export const GET_ALIAS = 'GET_ALIAS';

export function showAlias(alias) {

    return {
        type: GET_ALIAS,
        payload: alias
    }
}
