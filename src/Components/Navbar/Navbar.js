import React from 'react';
import PropTypes from 'prop-types';



class Navbar extends React.Component{

      state = {
            clicked: false,
            color: 'aqua',
            size: 20,
        };

    render(){
        console.log('---1');

        const {color, size} = this.state;

        return (
            <div style = {{color: this.state.color}}>
                Hello {color} and {size} {this.state.clicked.toString()}
                <button onClick = {this.handlerClick}>click me!</button>
            </div>
        );
    }

    handlerClick = () => {
        console.log('---1');

        console.log('---click');

            this.setState(this.state , () => {
                this.state.clicked = !this.state.clicked;
                if (this.state.color === "aqua") {
                    this.state.color = "blue"
                }
                else {
                    this.state.color = "aqua"
                }
            });
        this.props.onClick(this.state.clicked);
    };

}
    Navbar.propTypees = {
    color: PropTypes.object,
    };
Navbar.defaultTypees = {
    size: 30,
    };

export default Navbar;
