import { createStore, applyMiddleware } from 'redux';
import { rootReducer } from '../reducers';
import { ping } from './enhancers/ping'

const devToolsRedux = (window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export const store = createStore(
    rootReducer,[],
    applyMiddleware(ping)
);

