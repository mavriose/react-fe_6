import React from 'react';
import { connect } from 'react-redux';
import Navigation from "./MyButton/Navigation";
import Navbar from "./Components/Navbar/Navbar";
import {showAlias} from "./actions/ShowAlias";


class App extends React.Component {
  state  = {
    navbarClicked :false,
    color: this.props.color,
};

 render(){
     const { name, age } = this.props;
   return  (
       <div>
         <Navigation/>
         <h2>Name: {name.userName}<br/>Age: {age.userAge}</h2>
           <p> Alias: {name.alias}</p>
         <Navbar color = {this.state.color} onClick = {this.showSubmit.bind(this)} />
             {this.click ? <h1>Hello world</h1> : <h1>Or not Hello!</h1>}
             <button onClick={this.onBtnClick.bind(this)}>get alias</button>

       </div>)
 }
    onBtnClick() {
        this.props.getAlias('Eastsideguy');
        console.log(this.props);
    }
  showSubmit(click){
    console.log('---click from APP.js',  click.toString());
    this.click = click;
    this.setState(this.state, (click) => this.state.navbarClicked = click)
      console.log(this.props);

  };


}


const mapStateToProps = store => {
    console.log(store);
    return {
        name: store.userName,
        age: store.userAge,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getAlias: alias => dispatch(showAlias(alias)),
    }
};


export default connect (
    mapStateToProps,
    mapDispatchToProps
) (App);
