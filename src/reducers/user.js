import { GET_ALIAS } from '../actions/ShowAlias'
const initialState = {
    userName: 'Oleksii',
    alias: 'guest'
};

export function userReducer(state = initialState, action) {
    switch (action.type) {
        case GET_ALIAS:
            return {...state, alias: action.payload};

        default:
            return state
    }
}

