

import {combineReducers} from 'redux';
import {ageReducer} from "./age";
import {userReducer} from "./user";



export const rootReducer = combineReducers({
    userAge: ageReducer,
        userName: userReducer,
});